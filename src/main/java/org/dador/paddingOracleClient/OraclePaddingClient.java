package org.dador.paddingOracleClient;


import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;


/**
 * Main Class for Padding OracleClient
 */
public class OraclePaddingClient {
    static final String ENCRYPTED_MESSAGE = "5ca00ff4c878d61e1edbf1700618fb287c21578c0580965dad57f70636ea402fa0017c4acc82717730565174e2e3f713d3921bab07cba15f3197b87976525ce4";
    static final int BLOCK_SIZE = 16;

    /**
     * Fonction takes a number and creates a block of x00 values, padded according to PKCS#7
     * example : n=3 result is 00 00 .. 00 03 03 03
     * @param n : number of bytes of padding
     * @return byte[BLOCK_SIZE] filled with 0 and padding values
     */
    protected byte[] getPaddingArray(int n) {
        byte[] result = new byte[BLOCK_SIZE];
        int cpt=n;
        for(int i=BLOCK_SIZE-1;i>0;i--)
        {
        	if(cpt>0)
        	result[i]=(byte)n;
        	else
        	result[i]=(byte)0;
        	cpt--;
        }
        
   /*
         * TODO : Your CODE HERE
         */
        return result;
    }

    /**
     * Function that create a modified ciphertext bloc for trying a guess
     * Note that the "ciphertext" correspond to the IV part for the Block Cipher
     * @param ciphertext : original ciphertext bloc
     * @param decoded    : decrypted part of the plain text (for next bloc)
     * @param position   : position of the byte to guess
     * @param guess      : the guess for this query
     * @return a byte array with c0...c(i-1)||ci+i+g||cj+mj+i||...||cn+mn+i
     */
    protected byte[] buildGuessForPosition(byte[] ciphertext, byte[] decoded, int position, byte guess) {
        byte[] result = new byte[BLOCK_SIZE];

        /**
         * TODO : YOUR CODE HERE
         */
        
        
        

        return result;
    }

    /**
     * Fonction that splits a message into constituent blocs of BLOCK_SIZE
     *
     * @param message
     * @return an array of blocs
     * @throws IllegalArgumentException
     */
    protected byte[][] splitMessageIntoBlocks(byte[] message) throws IllegalArgumentException {
        if (message.length % BLOCK_SIZE != 0) {
            throw new IllegalArgumentException("Message length is not a multiple of bloc size");
        }
        int nbBlocks=(message.length/BLOCK_SIZE);
        byte [][]result=new byte [nbBlocks][BLOCK_SIZE];        
        int start = 0;
        
        for(int i = 0; i < nbBlocks; i++) {
            result[i] = Arrays.copyOfRange(message,start, start + BLOCK_SIZE);
            start += BLOCK_SIZE ;
        }
        return result;
    }

    /**
     * Function that takes the 2 last blocks of the message
     * and returns the length of the padding.
     * @param poq : a PaddingOracleQuery object
     * @param previousbloc : next to last block of the ciphertext
     * @param lastbloc : last bloc of the ciphertext
     * @return an integer corresponding to padding length
     * @throws IOException
     * @throws URISyntaxException
     */
    public int getPaddingLengthForLastBlock(PaddingOracleQuery poq, byte[] previousbloc, byte[] lastbloc) throws IOException, URISyntaxException {
        /**
         * TODO : Your Code HERE
         */
    	
        // should not arrive here !
        return 0;
    }

    /**
     * Main function that takes 2 consecutive blocks of the ciphertext
     * and returns the decryption of the 2nd message block
     * @param poq : a PaddingOracleQuery object to query server
     * @param iv : the "iv" part of the 2 blocks query
     * @param ciphertext : the block that will be decrypted
     * @param padding : set to 0 if not the last block. Set to paddinglength if last block
     * @return a decrypted byte array
     * @throws IOException
     * @throws URISyntaxException
     */
    public byte[] runDecryptionForBlock(PaddingOracleQuery poq, byte[] iv, byte[] ciphertext, int padding) throws IOException, URISyntaxException {
        byte[] decoded = new byte[BLOCK_SIZE];
        if (padding > 0) {
            decoded = getPaddingArray(padding);
        }
        /**
         * TODO : YOUR CODE HERE
         */
        String bloc1=HexConverters.toHexFromByteArray(iv);
        String bloc2=HexConverters.toHexFromByteArray(ciphertext);
        for(int i=0;i<256;i++)
        {
        	iv[15]=(byte) i;
        	bloc1=HexConverters.toHexFromByteArray(iv);
        	
        }
        
        
        
        return decoded;
    }

    public static void main(String[] args) throws IOException, URISyntaxException {
        OraclePaddingClient opc = new OraclePaddingClient();
        PaddingOracleQuery opq = new PaddingOracleQuery();
        HexConverters converter=new HexConverters();
        try {
            System.out.println("Server responded : " + opq.query(ENCRYPTED_MESSAGE));
        } catch (Exception e) {
            System.out.print("Exception caught. Server down ?");
            e.printStackTrace();
        }
        
        byte[][] testBlocs=opc.splitMessageIntoBlocks(converter.toByteArrayFromHex(ENCRYPTED_MESSAGE));
        	byte [] PremierOctet=testBlocs[0];
        	
        	String bloc2=converter.toHexFromByteArray(testBlocs[1]);
        	
        	PremierOctet[BLOCK_SIZE-1]=(byte)9;
        	String bloc1=converter.toHexFromByteArray(PremierOctet);
        	String Bloc12= bloc1+bloc2;
        	Boolean result=opq.query(Bloc12);
        		System.out.println(result);
        		
        		byte[] test=opc.getPaddingArray(5);
        		System.out.println(converter.toHexFromByteArray(test));
        		
    		
        	
        try {
            /**
             * TODO : YOUR CODE HERE
             * [BLOCK_SIZE-1]
             */
        	
        	
        	
        } catch (Exception e) {
            System.out.print("Exception caught. Server down ?");
            e.printStackTrace();
        }
    }

}

